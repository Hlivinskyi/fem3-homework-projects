//---------------------------------
//Our Services tabs switcher-------
//---------------------------------

const $tabItems = $(".services-tabs-item");
const $contentItems = $(".services-content-item");

$tabItems.on("click", () => {
    $(".active").removeClass("active");
    $(event.target).addClass("active");
    $contentItems.each((index, element) => {
        $(element).css({display: "none"});
        if($(element).data("content") === $(event.target).data("content")) {
            $(element).fadeIn();
            $(element).css({display: "flex"});
        }
    });
});

//---------------------------------
//Our Amazing Work tabs switcher---
//---------------------------------

const $tabWorkItems = $(".amazing-work-types-item");
const $photos = $(".amazing-work-photo");
$tabWorkItems.on("click", () => {
    $(".active-work").removeClass("active-work");
    $(event.target).addClass("active-work");
    // debugger;
    $photos.each((index, element) => {
        // debugger;
        $(element).css({display: "none"});
        $(element).fadeIn();
        $(element).css({display: "block"});
        if($(event.target).data("content") === "all"){
            console.log('1')
        }else if(!($(element).data("content") === $(event.target).data("content"))) {
            // $(element).fadeIn();
            console.log('2', element)
            $(element).css({display: "none"});
        }
    });
});

//---------------------------------
//Hover for images-----------------
//---------------------------------

const collectionOfPictures = document.querySelector(".amazing-work-photos");
let pictures = document.querySelectorAll(".amazing-work-photo");

const imgFirst = document.createElement("img");
const imgSecond = document.createElement("img");
imgFirst.src = "img/chainWhite.png";
imgSecond.src = "img/search-icon.png";

const circleFirst = document.createElement("div");
const circleSecond = document.createElement("div");
circleFirst.classList.add("circle-first");
circleSecond.classList.add("circle-second");

const circles = document.createElement("div");
circles.classList.add("circles");

const hoverBlock = document.createElement("div");
hoverBlock.classList.add("amazing-work-photo-hover");

const hoverTitle = document.createElement("p");
hoverTitle.classList.add("amazing-work-photo-hover-title");
hoverTitle.innerText = "creative design";
const hoverSubTitle = document.createElement("span");
hoverSubTitle.classList.add("amazing-work-photo-hover-subtitle");


circleFirst.append(imgFirst);
circleSecond.append(imgSecond);
circles.append(circleFirst, circleSecond);
hoverBlock.append(circles, hoverTitle, hoverSubTitle);

(callHover = () => {
    pictures.forEach((element) => {
        element.addEventListener("mouseover", (event) => {
            event.currentTarget.append(hoverBlock);
            let newString = event.currentTarget.dataset.content;

            newString = newString.replace("-", " ");
            hoverSubTitle.innerText = newString.toLowerCase()
                .split(' ')
                .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
                .join(' ');
            console.log(event.currentTarget.dataset);
        });
        element.addEventListener("mouseleave", () => {
            hoverBlock.remove();
        });
    });
})();


//---------------------------------
//Upload new photos----------------
//---------------------------------


const uploadPictures = document.getElementById("uploadPictures");
uploadPictures.addEventListener("click", () => {
    for(let i=1; i <= 12; i++){
        const newDiv = document.createElement('div');
        const picture = document.createElement("img");
        if(i <= 7){
            picture.src = `img/webDesign/web-design${i}.jpg`;
            newDiv.setAttribute("data-content","web-design");
        }else{
            picture.src = `img/landingPage/landing-page${i}.jpg`;
            newDiv.setAttribute("data-content","landing-pages");
        }
        newDiv.className = 'amazing-work-photo';
        newDiv.append(picture);
        collectionOfPictures.appendChild(newDiv);
        $photos.push(newDiv);

    }
    uploadPictures.remove();
    pictures = document.querySelectorAll(".amazing-work-photo");
    callHover();
});

//---------------------------------
//SLIDER---------------------------
//---------------------------------

$(".feedbacks-slider").slick({
    arrows: true,
    dots: true,
    prevArrow: '<div class="arrow left"></div>',
    nextArrow: '<div class="arrow right"></div>'
});

//---------------------------------
//MASONRY GALLERY------------------
//---------------------------------

const $gallery = $('#gallery-masonry');
const uploadMasonryButton = document.getElementById("uploadMansory");
let masonryItems  = document.getElementsByClassName("gallery-masonry-item");


$(document).ready(() => {

    $gallery.imagesLoaded(() => {
        $gallery.masonry({
            itemSelector: '.gallery-masonry-item',
            columnWidth: 1,
            percentPosition: true,
            gutter: 15,
            fitWidth: true
        });
    });


    uploadMasonryButton.addEventListener("click", () => {
        for(let i=1; i <= 12; i++){
            const newDiv = document.createElement('div');
            newDiv.classList.add("gallery-masonry-item");
            const picture = document.createElement("img");
            if(i <= 7){
                picture.src = `img/webDesign/web-design${i}.jpg`;
            }else{
                picture.src = `img/landingPage/landing-page${i}.jpg`;

            }
            newDiv.classList.add("sizer16");
            newDiv.append(picture);
            $gallery.imagesLoaded(() => {
                $gallery.append( newDiv ).masonry( 'appended', newDiv );
            });
            console.log(masonryItems.length);

        }
            setTimeout(() => {
                if(masonryItems.length >= 36){
                    uploadMansory.remove();
                }
                callHoverMasonry();
            }, 100);
    });


//---------------------------------
//Create hover for Masonry---------
//---------------------------------


    const $img1 = $('<img src="img/search-icon.png" alt="">');
    const $img2 = $('<img src="img/arrows.png" alt="">');
    const $buttonMasonry1 = $('<div class="masonry-hover-button1">');
    const $buttonMasonry2 = $('<div class="masonry-hover-button2">');
    $buttonMasonry1.append($img1);
    $buttonMasonry2.append($img2);
    const $hoverMasonry = $('<div class="masonry-hover">');
    $hoverMasonry.append($buttonMasonry1, $buttonMasonry2);

    (callHoverMasonry = () => {
        $('.gallery-masonry-item').each((index, element) => {
            $(element).on("mouseover", (event) => {
                $(event.currentTarget).append($hoverMasonry);
            });
            $(element).on("mouseleave", () => {
                $hoverMasonry.remove();
            });
        });
    })()
});