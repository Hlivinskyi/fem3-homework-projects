const images = Array.from(document.getElementsByClassName("image-to-show"));
const pauseButton = document.getElementById("pauseButton");
const playButton = document.getElementById("playButton");
let isPaused = false;

const repeatSlider = () => {
    let previous = images[0];
    let i = 1;
    setInterval(() => {
        if (!isPaused) {
            images[i].classList.add("active");
            if (previous) {
                previous.classList.remove("active");
            }
            previous = images[i];
            i++;
            if (i === images.length) {
                i = 0;
            }
        }
    }, 2000);
};

pauseButton.onclick = function() {
    debugger;
    isPaused = true;
};

playButton.onclick = function() {
    isPaused = false;
};

repeatSlider();