let priceBlock = document.querySelector(".price-block");
let priceField = document.getElementById("priceField");

let validator = document.createElement("div");
let validatorText = document.createElement("span");
let validatorClose = document.createElement("span");
let incorrectDataText = document.createElement("span");

validator.classList.add("price-block-validator");
validatorText.classList.add("price-block-validator-text");
validatorClose.classList.add("price-block-validator-close");
incorrectDataText.classList.add("incorrect-data");


priceField.onkeypress = function(event) {
    if(event.code === "Minus") {
        return true;
    }
    if(event.which < 48 || event.which > 57) {
        return false;
    }
};

priceField.onfocus = function() {
    priceField.classList.remove("red-border");
    priceField.classList.add("green-border");
};

priceField.onblur = function() {
    priceField.classList.remove("green-border");
    if(priceField.value === "") {
    }else if(priceField.value >= 0) {
        priceBlock.prepend(validator);
        priceField.style.color = "green";
        validatorText.innerText = `Current price: ${priceField.value}`;
        validator.appendChild(validatorText);
        validator.appendChild(validatorClose);
        incorrectDataText.remove();
    }else {
        priceField.classList.add("red-border");
        incorrectDataText.innerText = "Please enter correct price";
        priceBlock.appendChild(incorrectDataText);
        validator.remove();
    }
};

validatorClose.onclick = function () {
    validator.remove();
    priceField.value = "";
    priceField.style.color = "#434343";
};