// let tab1 = document.getElementById("tab1");
// let tab2 = document.getElementById("tab2");
// let tab3 = document.getElementById("tab3");
// let tab4 = document.getElementById("tab4");
// let tab5 = document.getElementById("tab5");
// const tabs = document.querySelectorAll(".tabs-title");
//
// let content1 = document.getElementById("content1");
// let content2 = document.getElementById("content2");
// let content3 = document.getElementById("content3");
// let content4 = document.getElementById("content4");
// let content5 = document.getElementById("content5");
// let tabsContent = document.getElementsByClassName("tabs-content-item");
//
//
// function viewChange(tabId, contentId) {
//     for(let i = 0; i < tabs.length; i++) {
//         tabs[i].classList.remove("active");
//     }
//     tabId.classList.add("active");
//     for(let i = 0; i < tabsContent.length; i++) {
//         tabsContent[i].hidden = true;
//     }
//     contentId.hidden = false;
// }
//
//
//
//
//
//
// tab1.addEventListener("click", function () {
//     viewChange(tab1,content1);
// });
//
// tab2.addEventListener("click", function () {
//     viewChange(tab2,content2);
// });
//
// tab3.addEventListener("click", function () {
//     viewChange(tab3,content3);
// });
//
// tab4.addEventListener("click", function () {
//     viewChange(tab4,content4);
// });
//
// tab5.addEventListener("click", function () {
//     viewChange(tab5,content5);
// });
//
// document.onclick = function (e) {
//     console.log(e);
// };

// const navBar = document.querySelector(".centered-content");
const navBar = document.querySelector(".tabs");
const tabs = Array.from(document.getElementsByClassName("tabs-title"));
const content = Array.from(document.getElementsByClassName("tabs-content-item"));

navBar.addEventListener("click", ({target}) => {
    tabs.forEach(element => {
        element.classList.remove('active')
    });
    content.forEach(element => {
        if(element.dataset.content === target.dataset.content) {
            element.classList.add('active')
        }else {
            element.classList.remove('active')
        }
    });
    target.classList.add('active');
});