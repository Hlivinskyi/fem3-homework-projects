$(document).ready(() =>{
    $("#menu").on("click", "a", function (event) {
        event.preventDefault();
        let id  = $(this).attr('href');
        let top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});

$(document).on("scroll", () => {
    const $screenHeigh = $(window).innerHeight();
    const $screenTop = $(window).scrollTop();
    if($screenHeigh < $screenTop) {
        if(!$(".scroll-top-button").length) {
            const $scrollTopButton = $("<button hidden class='scroll-top-button'></button>");
            const $arrow = $("<div class='arrow'></div>");
            $scrollTopButton.append($arrow);
            $("script:first").before($scrollTopButton);
            $scrollTopButton.fadeIn();
            $scrollTopButton.click(() => {
                $('body, html').animate({
                    scrollTop: 0
                }, 1000);
            })
        }
    }else{
        $(".scroll-top-button").fadeOut(500, () => {
            $(".scroll-top-button").remove();
        })
    }
});

$("#expandButton").on("click", () =>{
    debugger;
    if($(event.target).text() === "Expand Posts"){
        $(event.target).text("Collapse Posts");
    }else {
        $(event.target).text("Expand Posts");
    }
    $(".posts").slideToggle();
});