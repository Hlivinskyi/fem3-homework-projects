const student = {};

let firstName = prompt("Please enter your first name");
let lastName = prompt("Please enter your last name");

student.name = firstName;
student["last name"] = lastName;

student.table = {};

while (true) {
    let course = prompt("Please enter the course's name");
    if (!course) {
        break;
    }
    let grade = +prompt("Please enter the course's grade");
    student.table[course] = grade;
}

let badGrade = 0;

for(let grade in student.table) {
    if(student.table[grade] < 4) {
        badGrade++;
    }
}

if(badGrade === 0) {
    alert("Студент переведен на следующий курс");
} else {
    alert(`Количество плохих оценок ниже 4 баллов: ${badGrade}`);
}

let numberOfGrades = 0;
let sum = 0;

for(let grade in student.table) {
    sum += student.table[grade];
    numberOfGrades++;
}

let averageGrade = (sum/numberOfGrades).toFixed(2);

if(averageGrade > 7) {
    alert(`Средний балл по предметам: ${averageGrade}. Студенту назначена стипендия`);
} else {
    alert(`Средний балл по предметам: ${averageGrade}. Студенту НЕ назначена стипендия`);
}