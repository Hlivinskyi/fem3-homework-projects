const elementsArray = document.querySelectorAll(".btn");

function colorButtons() {
    document.addEventListener("keypress", (event) => {
        elementsArray.forEach(element => {
            element.style.backgroundColor = "#33333a";
            if(event.key === element.textContent) {
                element.style.backgroundColor = "blue";
            }
        })
    });
}

colorButtons();
