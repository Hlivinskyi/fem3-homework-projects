function calculator(firstNumber, secondNumber, operation) {
    let result;
    switch (operation) {
        case "+":
            result = +firstNumber + +secondNumber;
            break;
        case "-":
            result = firstNumber - secondNumber;
            break;
        case "*":
            result = firstNumber * secondNumber;
            break;
        case "/":
            result = firstNumber / secondNumber;
            break;
    }
    return result;
}

let number1 = prompt("Please enter the first number");

while (isNaN(number1) || number1 === "" || number1 === null) {
    if(number1 === null) {
        number1 = prompt("Invalid number. Please enter the first number", "");
    }else {
        number1 = prompt("Invalid number. Please enter the first number", number1);
    }
}

let number2 = prompt("Please enter the second number");

while (isNaN(number2) || number2 === "" || number2 === null) {
    if(number2 === null) {
        number2 = prompt("Invalid number. Please enter the second number", "");
    }else {
        number2 = prompt("Invalid number. Please enter the second number", number2);
    }
}

let operation = prompt("Please enter operation");

while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    operation = prompt("Operation is wrong. Please enter operation", operation);
}

console.log(calculator(number1, number2, operation));