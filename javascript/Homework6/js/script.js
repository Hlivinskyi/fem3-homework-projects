const filterBy = (array, dataType) => {
    let newArray = array
        .map(element => {
            if(typeof(element) !== dataType) {
                return element;
            }
        })
        .filter(element => element !== undefined);
    return newArray;
};

let array = ['hello', 'world', 23, '23', null];
console.log(filterBy(array, "string"));