let name = prompt("Please enter your name");

while(name === null || name === "") {
    name = prompt("You did not enter an invalid name. Please enter your name again", "");
}

let age = prompt("Please enter your age");

while(isNaN(age) || age === null || age === "") {
    if(age === null){
        age = prompt("You did not enter an invalid age. Please enter your age again.", "");
    } else {
        age = prompt("You did not enter an invalid age. Please enter your age again.", age);
    }
}

if(age < 18) {
    alert("You are not allowed to visit this website");
} else if(age <= 22) {
    let isAgeValid = confirm("Are you sure you want to continue?");
    isAgeValid ? alert(`Welcome, ${name}`) : alert("You are not allowed to visit this website");
} else {
    alert(`Welcome, ${name}`);
}