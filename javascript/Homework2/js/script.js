//Task 1 --------------------------------------------------------------
let number = +prompt("Please enter the integer number");

while(!Number.isInteger(number)){
    number = +prompt("Number is not integer. Please enter the integer number");
}

let count = 0;

for(let i = 1; i <= number; i++){
    if(i % 5 === 0) {
        console.log(i);
        ++count;
    }
}
if(number === 0 || count === 0) {
    console.log("Sorry, no numbers");
}
if(Math.sign(number) === -1){
    console.log("Please enter only positive number");
}

//Task 2 --------------------------------------------------------------
let number1 = +prompt("Please enter the first integer number");

while(!Number.isInteger(number1)){
    number1 = +prompt("Number is not integer. Please enter the integer number");
}

let number2 = +prompt("Please enter the second integer number");

while(!Number.isInteger(number2)){
    number2 = +prompt("Number is not integer. Please enter the integer number");
}

while(number1 >= number2) {
    alert("Second number should be more than first");
    number1 = +prompt("Please enter the first integer number");
    number2 = +prompt("Please enter the second integer number");
    while(!Number.isInteger(number1) || !Number.isInteger(number2)){
        number1 = +prompt("Number is not integer. Please enter the first integer number");
        number2 = +prompt("Number is not integer. Please enter the second integer number");
    }
}

//Implementation with labels
outer: for(; number1 <= number2; number1++) {
    if (number1 === 1) {}
    else if(number1 === 2) {
        console.log(number1);
    } else {
        for(let x = 2; x < number1; x++) {
            if(number1 % x === 0) {
                continue outer;
            }
        }
        console.log(number1);
    }
}

//Implementation with while loop
for(; number1 <= number2; number1++) {
    let x = 2;
    if (number1 === 1) {}
    else if(number1 === 2) {
        console.log(number1);
    } else {
        while(number1 >= x) {
            if(number1 === x) {
                console.log(number1);
                break;
            }
            if(number1 % x === 0) {
                break;
            }
            x++;
        }
    }
}