
//Mandatory Task
function convertToList(array) {
    const list = document.createElement("ul");
    let newArray = array.map(element => {
        let li = document.createElement("li");
        li.innerText = element;
        return li;
    });
    for(let element of newArray) {
        list.appendChild(element);
    }
    document.querySelector("script").before(list);
}

//Additional Task
// function convertToList(array) {
//     let list = document.createElement("ul");
//     let newArray = array.map(element => {
//         debugger;
//         if(Array.isArray(element)) {
//             let li = document.createElement("li");
//             li.appendChild(document.createElement("ul"));
//             return convertToList(element);
//         }else {
//             let li = document.createElement("li");
//             li.innerText = element;
//             return li;
//         }
//         })
//         .filter(element => element !== undefined);
//     for(let element of newArray) {
//         list.appendChild(element);
//     }
//     document.querySelector("script").before(list);
// }

convertToList([[1,2,3], [5,6,["w", "q"]], 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
convertToList(['1', '2', '3', 'sea', 'user', 23]);

