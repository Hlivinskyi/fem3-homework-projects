const toursItemPrice = Array.from(document.getElementsByClassName("tours-item-price"));
const switchButton = document.getElementById("switchButton");
const toursName = Array.from(document.getElementsByClassName("tours-item-name-text"));
const headerLogoTitle = document.querySelector(".header-logo-title");
const headerLogoSubtitle = document.querySelector(".header-logo-subtitle");
const headerLogoImg = document.querySelector(".header-logo-img");


const applyChanges = () => {
    let color = localStorage.getItem("color");
    switchButton.style.backgroundColor = `${color}`;
    let img = localStorage.getItem("img");
    headerLogoImg.style.content = img;
    toursItemPrice.forEach((element) => {
        element.style.backgroundColor = `${color}`;
    });
    toursName.forEach((element) => {
        element.style.color = `${color}`;
    });
    headerLogoTitle.style.color = `${color}`;
    headerLogoSubtitle.style.color = `${color}`;
};


switchButton.onclick = () => {
    if(localStorage.getItem("color") === "deepskyblue"){
        localStorage.setItem("color", "#f26522");
        localStorage.setItem("img", "url(\"img/building-icon-orange.png\")");
        applyChanges();
    }else {
        localStorage.setItem("color", "deepskyblue");
        localStorage.setItem("img", "url(\"img/building-icon-blue.png\")");
        applyChanges();
    }
};


const uploadCurrentTheme = () => applyChanges();

uploadCurrentTheme();