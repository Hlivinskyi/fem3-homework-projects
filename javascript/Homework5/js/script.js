const createNewUser = () => {
    let userFirstName = prompt("Please enter your first name");
    let userLastName = prompt("Please enter your second name");
    let userBirthday = prompt("Please enter your birthday according to \"dd.mm.yyyy\" format");
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        birthday: userBirthday,
        getLogin() {
            return (userFirstName.charAt(0) + userLastName).toLowerCase();
        },
        getAge() {
            let parseDate = userBirthday.split(".");
            let birthDate = new Date(`${parseDate[1]}/${parseDate[0]}/${parseDate[2]}`);
            let today = new Date();
            let age = today.getFullYear() - birthDate.getFullYear();
            if(today.getMonth() < birthDate.getMonth() || today.getMonth() === birthDate.getMonth() && today.getDate() < birthDate.getDate()) {
                return --age;
            }
            return age;
        },
        getPassword() {
            let parseDate = userBirthday.split(".");
            return userFirstName[0].toUpperCase() + userLastName.toLowerCase() + parseDate[2];
        }

    };
    return newUser;
};

const user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());