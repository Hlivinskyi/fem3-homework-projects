//Mandatory Task
const createNewUser = () => {
    let userFirstName = prompt("Please enter your first name");
    let userLastName = prompt("Please enter your second name");
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        getLogin() {
            return (userFirstName.charAt(0) + userLastName).toLowerCase();
        },
    };
    return newUser;
};

const user = createNewUser();
console.log(user.getLogin());



//Optional Task
const createNewUserOptional = () => {
    let firstName = prompt("Please enter your first name");
    let lastName = prompt("Please enter your second name");
    const newUser = {
        setFirstName(value) {
            firstName = value;
        },
        setLastName(value) {
            lastName = value;
        },
        getFirstName() {
            return firstName;
        },
        getLastName() {
            return lastName;
        },
        getLogin() {
            return (firstName.charAt(0) + lastName).toLowerCase();
        },
    };
    return newUser;
};

const userOptional = createNewUserOptional();
console.log(userOptional.getLogin());
console.log(userOptional.getFirstName() + " " + userOptional.getLastName());
userOptional.setFirstName("Lisa");
userOptional.setLastName("Stevens");
console.log(userOptional.getFirstName() + " " + userOptional.getLastName());