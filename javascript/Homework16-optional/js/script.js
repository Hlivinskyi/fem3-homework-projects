// Iterative method
function factorial(number) {
    let result = 1;
    let i = 1;
    while (i <= number) {
        result = result * i++;
    }
    return result;
}

// Recursive method
let factorialRec = function(number) {
    if(number <= 1) {
        return 1;
    }else {
        return number * factorialRec(number - 1);
    }
};

let number = +prompt("Please enter the number", "1");

while (isNaN(number) || number === "" || number === null || number === 0) {
    number = prompt("Incorrect number. Please enter the number", "1");
}

alert(`Factorial of number: ${number} is ${factorial(number)}`);
alert(`Factorial of number: ${number} is ${factorialRec(number)}`);