const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirmPassword");
const passwordIcon = document.getElementById("passwordIcon");
const passwordConfirmIcon = document.getElementById("passwordConfirmIcon");
const confirmButton = document.getElementById("button");
const validatorMessage = document.createElement("p");

passwordIcon.onclick = function () {
    passwordIcon.classList.toggle("fa-eye-slash");
    if(passwordIcon.classList.contains("fa-eye-slash")) {
        password.type = "text";
    }else {
        password.type = "password";
    }
};

passwordConfirmIcon.onclick = function () {
    passwordConfirmIcon.classList.toggle("fa-eye-slash");
    if(passwordConfirmIcon.classList.contains("fa-eye-slash")) {
        confirmPassword.type = "text";
    }else {
        confirmPassword.type = "password";
    }
};

confirmButton.onclick = function () {
    if(password.value === "" || confirmPassword.value ==="") {
        alert("Please enter the values");
        debugger;
    }else if(password.value === confirmPassword.value) {
        validatorMessage.remove();
        alert("You are welcome");
    }else {
        if(!validatorMessage.isPresenting) {
            validatorMessage.innerText = "The same values should be entered";
            validatorMessage.style.color = "red";
            confirmPassword.insertAdjacentElement("afterend", validatorMessage);
        }
    }
    return false;
};



