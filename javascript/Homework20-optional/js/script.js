const workingDays = (deadline) => {
    let today = new Date();
    let dateOfDeadline = new Date(deadline);
    let workingDaysToDeadline = [];
    for (;today <= dateOfDeadline; today.setDate(today.getDate() + 1)) {
        if(today.getDay() !== 0 && today.getDay() !== 6) {
            workingDaysToDeadline.push(new Date(today));
        }
    }
    return workingDaysToDeadline.length;
};

const timeEstimation = (team, tasks, deadline) => {
    let teamCapacityPerDay = 0;
    let tasksLoad = 0;
    for(let manCapacity of team) {
        teamCapacityPerDay += manCapacity;
    }
    for(let taskLoad of tasks) {
        tasksLoad += taskLoad;
    }
    let daysNeeded = Math.ceil(tasksLoad / teamCapacityPerDay);
    if(workingDays(deadline) - daysNeeded >= 0) {
        alert(`Все задачи будут успешно выполнены за ${daysNeeded} дней до наступления дедлайна!`);
    }else {
        let hoursLeft = (daysNeeded - workingDays(deadline)) * 8;
        alert(`Команде разработчиков придется потратить дополнительно ${hoursLeft} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
    }
};

timeEstimation([1,2,3], [10,10,10], "08.20.2019");
timeEstimation([1,2,3], [10,10,10,10,10], "08.20.2019");




