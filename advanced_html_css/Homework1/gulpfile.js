const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();

const path = {
    dist: {
        html: 'prod/',
        css: 'prod/css',
        js: 'prod/js',
        img: 'prod/img',
        self: 'prod/**/*'
    },
    src : {
        html: 'src/*.html',
        scss: 'src/scss/*.scss',
        js: 'src/js/*.js',
        img: 'src/img/*.*'
    }
};

/************* F U N C T I O N S ********************/

const resetBuild = () => (
    gulp.src('src/css/reset.css')
        .pipe(gulp.dest('prod/css/'))
);

const imgBuild = () => (
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.dist.img))
);

const htmlBuild = () => (
    gulp.src('src/index.html')
        .pipe(gulp.dest('prod/'))
);

const scssBuild = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(path.dist.css))
);

const jsBuild = () => (
    gulp.src(path.src.js)
        .pipe(concat('script.js'))
        .pipe(gulp.dest(path.dist.js))
);

const cleanDist = () => (
    gulp.src(path.dist.self, {allowEmpty: true})
        .pipe(clean())
);

/************* W A T C H E R S ********************/
const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "prod/"
        }
    })
};
gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);




/************* T A S K S ********************/

gulp.task('default', gulp.series(
    cleanDist,
    gulp.parallel(resetBuild, htmlBuild, scssBuild, imgBuild),
    watcher
));